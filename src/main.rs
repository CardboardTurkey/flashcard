use anyhow::Result;
use clap::Parser;
use std::{
    collections::{BTreeMap, HashMap},
    fs,
    path::{Path, PathBuf},
};

mod cli;
mod printer;

use cli::Options;
use printer::Printer;

type FlashCard = HashMap<String, HashMap<String, String>>;
type ScoreCard = BTreeMap<String, BTreeMap<String, bool>>;

fn load_flashcards(flashcard_file: &PathBuf, topics: &[String]) -> Result<FlashCard> {
    let mut flashcards: FlashCard = serde_yaml::from_reader(std::fs::File::open(flashcard_file)?)?;
    let topics: Vec<String> = topics
        .iter()
        .map(|x| serde_yaml::from_str(x))
        .collect::<Result<Vec<String>, serde_yaml::Error>>()?;
    if !topics.is_empty() {
        flashcards = flashcards
            .into_iter()
            .filter(|x| topics.contains(&x.0))
            .collect();
    };
    Ok(flashcards)
}

fn load_scorecard(scorecard_file: &Path) -> Result<ScoreCard> {
    let file_contents = match fs::File::open(&scorecard_file) {
        Ok(rdr) => serde_yaml::from_reader(rdr),
        Err(ref e) if e.kind() == std::io::ErrorKind::NotFound => Ok(ScoreCard::new()),
        Err(e) => Err(e)?,
    }?;
    Ok(file_contents)
}

fn run_flashcard(options: &Options) -> Result<()> {
    let flashcards = load_flashcards(&options.flashcard_file, &options.topics)?;
    if options.list {
        println!("{:?}", flashcards.keys());
        return Ok(());
    }
    let mut scorecard_path = options.flashcard_file.to_path_buf();
    scorecard_path.set_extension("scorecard.yml");
    let mut scorecard = load_scorecard(&scorecard_path)?;
    let mut printer = Printer::new(&flashcards, &mut scorecard);
    printer.print_flashcards(options.filter)?;
    serde_yaml::to_writer(std::fs::File::create(scorecard_path)?, printer.scorecard())?;
    Ok(())
}

fn main() {
    let options = Options::parse();
    if let Err(err) = run_flashcard(&options) {
        eprintln!("{:?}", err);
    }
}
